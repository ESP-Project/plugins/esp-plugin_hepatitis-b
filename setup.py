'''
                                  ESP Health
                                 Hepatitis B
                             Packaging Information


@author: Jason McVetta <jason.mcvetta@gmail.com>
@organization: Channing Laboratory http://www.channing.harvard.edu
@contact: http://www.esphealth.org
@copyright: (c) 2011-2012 Channing Laboratory
@license: LGPL
'''

from setuptools import find_packages
from setuptools import setup

setup(
    name='esp-plugin_hepatitis-b',
    version='2.1',
    author='Chaim Kirby',
    author_email='ckirby@commoninf.com',
    description='Hepatitis B disease definition module for ESP Health application',
    license='LGPLv3',
    keywords='hepatitis b algorithm disease surveillance public health epidemiology',
    url='http://esphealth.org',
    packages=find_packages(exclude=['ez_setup']),
    install_requires=[
    ],
    entry_points='''
        [esphealth]
        disease_definitions = hepatitis_b:disease_definitions
        event_heuristics = hepatitis_b:event_heuristics
    '''
)
